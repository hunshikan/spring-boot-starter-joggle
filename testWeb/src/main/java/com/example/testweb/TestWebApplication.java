package com.example.testweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class TestWebApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(TestWebApplication.class, args);

    }

}
