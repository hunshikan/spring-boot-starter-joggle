package com.example.testweb.controller;

import org.apache.commons.codec.binary.Hex;
import org.hnzyx.joggle.algorithm.RSAUtil;
import org.hnzyx.joggle.annotations.JoggelApi;
import org.hnzyx.joggle.api.ForeignFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
//@RequestMapping("/hhh")
public class TestController {

    @Autowired
    private ForeignFunction foreignFunction;

    static Integer i =0;


    @DeleteMapping("/api/delete/{id}")
    @JoggelApi
    public String delete(@PathVariable("id") Integer id){
        return "user Input2: " + id ;
    }

    @PostMapping("/api/122")
    @JoggelApi
    public String get1(@RequestParam(value = "context",required = false) String context,
                       @RequestParam(value = "age",required = false) String age,
                       @RequestParam(value = "name",required = false) String name){
        return "user Input2: " + context + "age" + age + "name:" + name;
    }
    @RequestMapping("/api/3")
    @JoggelApi
    public String ssss(@RequestParam(value = "context",required = false) String context,
                       @RequestParam(value = "age",required = false) String age,
                       @RequestParam(value = "name",required = false) String name){
        return "user Input2: " + context + "age" + age + "name:" + name;
    }

    @PostMapping({"/api/34","/12312312"})
    @JoggelApi
    public String sssssss(@RequestParam(value = "context",required = false) String context,
                          @RequestParam(value = "age",required = false) String age,
                          @RequestParam(value = "name",required = false) String name){
        return "user Input2: " + context + "age" + age + "name:" + name;
    }

    @PostMapping("/api/222222222")
    @JoggelApi
    public String sss(@RequestParam(value = "context",required = false) String context,
                       @RequestParam(value = "age",required = false) String age,
                       @RequestParam(value = "name",required = false) String name){
        return "user Input2: " + context + "age" + age + "name:" + name;
    }

    @GetMapping("/test1")
    public String test(@RequestParam String context , @RequestParam String publicKye) throws Exception {
        String encrypt = RSAUtil.encrypt("{\"name\":\"123\",\"age\":123,\"address\":null,\"phone\":\"13187052997\"}", "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDM36OPZPM+IxNPcZuWL9yzTSLqvtSVRA/pOQ+zPO2asyTqH04YdPvQdmhnHJgf5QuR+ST0QlLApiZU9bo8dNFjTj6sZL6c9dfTOXzduDhCB8NJ/8jG8xcTv9oBQRPxlxUKDfy2FQpjcKqrWn+rvV7MjAVnFhLor2wqzy2UXC3iSwIDAQAB");
        return encrypt;
    }

    @GetMapping("/test2")
    public String[] tes2() throws Exception {
        return foreignFunction.getUserPublicKey();
    }

    @GetMapping("/api/get")
    public String get(@RequestParam(value = "context",required = false) String context){
        return "user Input1: " + context;
    }

    @GetMapping("/api/get1")
    @JoggelApi
    public String get1231(@RequestParam(value = "context",required = false) String context,
                       @RequestParam(value = "age",required = false) String age,
                       @RequestParam(value = "name",required = false) String name){
        return "user Input2: " + context + "age" + age + "name:" + name;
    }

    @PostMapping("/api/post")
    public String post(@RequestBody Map<String,String> map){
        System.out.println("user Input2: " + i + System.currentTimeMillis() + ":" + map);
        return "user Input2: " + i + System.currentTimeMillis() + ":" + map;
    }

    @GetMapping("/api/hello")
    public String hello(@RequestParam("hello") String hello){
        return "hello world"+hello ;
    }
}
