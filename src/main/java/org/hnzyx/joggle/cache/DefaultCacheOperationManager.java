package org.hnzyx.joggle.cache;

import org.hnzyx.joggle.config.RedisManager;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;

public class DefaultCacheOperationManager implements CacheOperationManager{


    @Autowired
    private RedisManager redisManager;

    @Override
    public String getKey(String key) {
        try (
                Jedis jedis = redisManager.getJedis();
                ){
            return jedis.get(key);
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public String setKey(String key, String value, Long duration) {
        Jedis jedis = redisManager.getJedis();
        String setex = jedis.setex(key, Long.valueOf(duration).intValue(), value);
        return setex;
    }

}
