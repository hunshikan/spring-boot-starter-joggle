package org.hnzyx.joggle.cache;

/**
 * 缓存处理器
 * */
public interface CacheOperationManager {


    /**
     * <p>
     *     通过指定key获取值对应的值
     * </p>
     * */
    String getKey(String key);

    /**
     * <p>
     *     设置key value 以及过期时间
     * </p>
     * */
    String setKey(String key,String value,Long duration);

}
