package org.hnzyx.joggle.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Component
@Target(value = {ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JoggelApi {
}
