package org.hnzyx.joggle.config;

import org.hnzyx.joggle.common.URIS;
import org.hnzyx.joggle.filter.JoggleInterceptor;
import org.hnzyx.joggle.filter.WebUrlIntercept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;


public class JoggleWebMvcConfigurer implements WebMvcConfigurer {



    @Autowired
    private CustomProperties joggleProperties;

    @Autowired
    private List<JoggleInterceptor> joggleInterceptors;

    @Autowired
    private WebUrlIntercept webUrlIntercept;

    @Autowired
    private URIS uris;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 设置拦截地址
        List<String> prefixs = uris.getUrls();
        if(joggleInterceptors.size() == 0){
            // 默认过滤器
            registry.addInterceptor(webUrlIntercept);
        } else {
            // 将容器中所有实现 JoggleInterceptor 拦截器加入其中
            for (JoggleInterceptor joggleInterceptor : joggleInterceptors) {
                this.setInterceptorRegistration(registry.addInterceptor(joggleInterceptor), prefixs);
            }
        }
    }

    /** 为拦截器设置拦截地址*/
    public void setInterceptorRegistration(InterceptorRegistration ir, List<String> profixs){
        ir.addPathPatterns(profixs);
    }
}
