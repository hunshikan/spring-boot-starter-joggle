package org.hnzyx.joggle.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("spring.joggle")
public class CustomProperties {

    private String prefix;

    @Value("${spring.joggle.url.enable}")
    private String urlEnable;

    @Value("${spring.joggle.redis.total:20}")
    private String redisTotal;

    @Value("${spring.joggle.redis.minidle:20}")
    private String minidle;
    @Value("${spring.joggle.redis.maxidle:20}")
    private String maxidle;
    @Value("${spring.joggle.redis.ip:127.0.0.1}")
    private String redisIp;
    @Value("${spring.joggle.redis.port:6379}")
    private String redisPort;

    private Long replayTime = 120000l;

    public Long getReplayTime() {
        return replayTime;
    }

    public void setReplayTime(Long replayTime) {
        this.replayTime = replayTime;
    }

    public String getUrlEnable() {
        return urlEnable;
    }

    public void setUrlEnable(String urlEnable) {
        this.urlEnable = urlEnable;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getRedisTotal() {
        return redisTotal;
    }

    public void setRedisTotal(String redisTotal) {
        this.redisTotal = redisTotal;
    }

    public String getMinidle() {
        return minidle;
    }

    public void setMinidle(String minidle) {
        this.minidle = minidle;
    }

    public String getMaxidle() {
        return maxidle;
    }

    public void setMaxidle(String maxidle) {
        this.maxidle = maxidle;
    }

    public String getRedisIp() {
        return redisIp;
    }

    public void setRedisIp(String redisIp) {
        this.redisIp = redisIp;
    }

    public String getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(String redisPort) {
        this.redisPort = redisPort;
    }
}
