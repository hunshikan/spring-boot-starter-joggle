package org.hnzyx.joggle.config;

import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;

public class RedisManager {

    @Autowired
    private CustomProperties customProperties;

    private static JedisPool pool;

    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

    @PostConstruct
    private void initRedis(){

        Integer maxTotal = Integer.parseInt(customProperties.getRedisTotal());
        Integer maxIdle = Integer.parseInt(customProperties.getMaxidle());
        Integer minIdle = Integer.parseInt(customProperties.getMinidle());
        String ip = customProperties.getRedisIp();
        Integer port = Integer.parseInt(customProperties.getRedisPort());

        // 初始化jedis
        jedisPoolConfig.setMaxTotal(maxTotal);
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMinIdle(minIdle);
        jedisPoolConfig.setMaxWaitMillis(100);
        jedisPoolConfig.setTestOnBorrow(false);//jedis 第一次启动时，会报错
        jedisPoolConfig.setTestOnReturn(true);
        // 初始化JedisPool
        pool = new JedisPool(jedisPoolConfig, ip, port, 1000);
    }


    public Jedis getJedis(){
        return pool.getResource();
    }
}
