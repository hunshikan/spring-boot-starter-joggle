package org.hnzyx.joggle.api;

import org.hnzyx.joggle.algorithm.RSAUtil;
import org.hnzyx.joggle.cache.DefaultCacheOperationManager;
import org.hnzyx.joggle.common.CacheKye;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.UUID;

public class ForeignFunction {

    @Autowired
    private DefaultCacheOperationManager defaultCacheOperationManager;

    @Autowired
    private RSAUtil rsaUtil;


    // 生产用户密钥公钥
    public String[] getUserPublicKey() throws Exception {
        String appKye = UUID.randomUUID().toString().replace("-", "");
        String[] keys = rsaUtil.genKeyPair();
        // 生产用户的密钥 存放到redis
        System.out.println(keys[1]);
        defaultCacheOperationManager.setKey(CacheKye.joggleSecret + appKye
                ,keys[1],
                Long.valueOf(10000));
        return new String[]{keys[0],appKye};
    }

    public Boolean DestructionUserPublicKye(String userUnique,String appKey){
        return Boolean.TRUE;
    }


}
