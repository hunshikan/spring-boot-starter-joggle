package org.hnzyx.joggle.common;

public final class CommonVariable {


    // 重放随机数
    public final static String NONCE = "nonce";

    // 接口签名
    public final static String SIGN = "sign";

    // 用户密钥标识
    public final static String ACCESS_KEY = "accessKey";

    // get请求加密的参数
    public final static String PARAMETER = "parameter";

    // 接口是否验证标识
    public static final String ADOPT = "adopt";

    // 请求时间
    public static final String REQUEST_TIME= "requestTime";


    public static final String OK = "OK";

    // 参数拼接值
    public final static String EQ = "=";

    // 参数拼接值
    public static final String WHAT = "?";

    // 参数拼接值
    public static final String JOIN = "&";

    // 参数拼接值
    public static final String MO = ":";

}
