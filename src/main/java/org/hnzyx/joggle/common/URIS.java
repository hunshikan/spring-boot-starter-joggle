package org.hnzyx.joggle.common;

import org.hnzyx.joggle.annotations.JoggelApi;
import org.hnzyx.joggle.filter.JoggleInterceptor;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class URIS {

    public URIS() {
    }

    // 所有标记组件的请求地址 例子  GET /api/test
    private static List<String> urls = new ArrayList<String>();


    public List<String> getUrls(){
        return this.urls;
    }

    public URIS(Map<String, Object> beans){
        // 找到所有标记注解的请求地址
        for (Map.Entry<String,Object> be : beans.entrySet()){
            String[] prefix = new String[]{};
            Object value = be.getValue();
            RequestMapping req = value.getClass().getAnnotation(RequestMapping.class);
            if (Objects.nonNull(req)){
                value = req.value();

            }
            Method[] methods = value.getClass().getMethods();
            for (Method method : methods) {
                JoggelApi ja = method.getAnnotation(JoggelApi.class);
                if (Objects.nonNull(ja)){
                    urls.addAll(this.parsingAnnotation(prefix, method));
                }
            }
        }
        System.out.println(urls);
    }


    public List<String> parsingAnnotation(String prefix[],Method method){
        List<String> methodUrl = new ArrayList<>();
        if (Objects.nonNull(method.getAnnotation(GetMapping.class))){
            this.setMethodList(methodUrl, method.getAnnotation(GetMapping.class).value(), prefix, JoggleInterceptor.METHOD_GET);
            return methodUrl;
        }

        if (Objects.nonNull(method.getAnnotation(PostMapping.class))){
            this.setMethodList(methodUrl, method.getAnnotation(PostMapping.class).value(), prefix, JoggleInterceptor.METHOD_POST);
            return methodUrl;
        }

        if (Objects.nonNull(method.getAnnotation(PutMapping.class))){
            this.setMethodList(methodUrl, method.getAnnotation(PutMapping.class).value(), prefix, JoggleInterceptor.METHOD_PUT);
            return methodUrl;
        }

        if (Objects.nonNull(method.getAnnotation(DeleteMapping.class))){
            this.setMethodList(methodUrl, method.getAnnotation(DeleteMapping.class).value(), prefix, JoggleInterceptor.METHOD_DELETE);
            return methodUrl;
        }

        if (Objects.nonNull(method.getAnnotation(RequestMapping.class))){
            this.setMethodList(methodUrl, method.getAnnotation(RequestMapping.class).value(), prefix, JoggleInterceptor.METHOD_DELETE);
            return methodUrl;
        }
        return methodUrl;
    }

    public void setMethodList(List<String> methodUrl, String[] values, String prefixs[], String mets){
        for (String value : values) {
            if (prefixs.length > 0){
                for (String prefix : prefixs) {
                    methodUrl.add(prefix + value);
                }
            } else {
                methodUrl.add(value);
            }
        }
    }
}
