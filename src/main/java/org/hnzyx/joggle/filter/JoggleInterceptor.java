package org.hnzyx.joggle.filter;

import org.springframework.web.servlet.HandlerInterceptor;

public class JoggleInterceptor implements HandlerInterceptor {
    public static final String METHOD_DELETE = "DELETE";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
}
