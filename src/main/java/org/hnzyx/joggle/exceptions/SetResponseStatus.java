package org.hnzyx.joggle.exceptions;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SetResponseStatus {

    public void setStatus(Exception e, HttpServletResponse response){
        try {
            if (e instanceof NoPermissionException){
                // 	请求要求用户的身份认证
                response.setStatus(401);
            } else if (e instanceof SignErrorException){
                // 客户端请求信息的先决条件错误
                response.setStatus(412);
            } else if (e instanceof RefuseRequestException){
                // 检测为重放提交 拒绝访问
                response.setStatus(452);
            } else if (e instanceof ErrorException){
                // 内部处理错误
                response.setStatus(400);
            } else if (e instanceof RequestTimeErrorException){
                // 请求时间超时
                response.setStatus(543);
            } else if (e instanceof ReplayRequestException){
                // 检测为重放提交 拒绝访问
                response.setStatus(452);
            } else {
                // 客户端请求的语法错误，服务器无法理解 默认错误
                response.setStatus(400);
            }
        } catch (Exception ioException) {
            ioException.printStackTrace();
            response.setStatus(400);
        }
    }
}
